import React from "react";

import "./Attribution.css";
import Icon from "../../components/Icon";

const Attribution = () => {
  return (
    <div className="attribution">
      Personnal challenge. Coded by <span>Massimo Bayle</span>
      <a className="social-icon" href="https://github.com/massiwo/" target="_blank" title="Github">
        <Icon iconName="GithubIcon" />
      </a>
      <a
        className="social-icon"
        href="https://bitbucket.org/smako/"
        target="_blank"
        title="Bitbucket"
      >
        <Icon iconName="BitbucketIcon" />
      </a>
      ©2020
    </div>
  );
};

export default Attribution;
