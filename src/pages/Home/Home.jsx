import React, { useEffect, useState } from "react";
import "./Home.css";

import firebase from "firebase";
import db from "../../config/firebase";
import Message from "../../components/Message";
import Icon from "../../components/Icon";
import FlipMove from "react-flip-move";
import Attribution from "../../components/Attribution";

const MAX_MESSAGE = 6;

function Home() {
  const [error, setError] = useState(false);
  const [username, setUsername] = useState("");
  const [messages, setMessages] = useState([]);
  const [input, setInput] = useState("");

  const sendMessage = () => {
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();

    db.collection("messages").add({
      username: username,
      text: input,
      timestamp,
    });
    setInput("");
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (error === false && input !== "") {
      const ref = db.collection("messages").orderBy("username").where("username", "!=", "Massimo");

      const snap = await ref.get();
      if (snap.size > MAX_MESSAGE) {
        await snap.docs
          .sort((a, b) => b.data().timestamp - a.data().timestamp)
          .slice(MAX_MESSAGE - 1, snap.docs.length - 1)
          .forEach((doc) => doc.ref.delete());
      }
      sendMessage();
    }
  };

  /* Remove older data */

  /* Fetch data */
  useEffect(() => {
    db.collection("messages")
      .orderBy("timestamp", "desc")
      .onSnapshot((snapshot) => {
        setMessages(snapshot.docs.map((doc) => ({ id: doc.id, message: doc.data() })));
      });
  }, []);

  /* Ask the username */
  useEffect(() => {
    const name = prompt(`Veuillez entrer votre pseudonyme`);
    if (
      name === "" ||
      name.toLowerCase().includes("massimo") ||
      name.toLowerCase().includes("bayle")
    ) {
      setUsername("Me");
    } else {
      setUsername(name);
    }
  }, []);

  return (
    <div className="home">
      <div className="header">
        <img
          src="https://facebookbrand.com/wp-content/uploads/2018/09/Header-e1538151782912.png?w=64&h=64"
          alt=""
        />
        <h1>Facebook Messenger</h1>
      </div>
      <div className="device">
        {/** Entête */}
        <div className="header">
          <div className="header-info"></div>
          <div className="header-options"></div>
        </div>
        {/** Messages chat */}
        <div className="chat">
          <FlipMove>
            {username !== "" &&
              messages.map(({ id, message }) => (
                <Message key={id} username={username} message={message} />
              ))}
          </FlipMove>
        </div>
        <div className="chat__input" onSubmit={onSubmit}>
          {error && <label className="chat__error">Veuillez renseigner un pseudo</label>}
          {!error && username && (
            <label className="chat__name" htmlFor="">
              Your name is "{username}"
            </label>
          )}
          <form actototion="">
            <input
              disabled={error}
              type="text"
              value={input}
              placeholder='Write and press "Enter"'
              onChange={(e) => setInput(e.target.value)}
            />
            <button type="submit">
              <Icon iconName="PaperPlaneIcon" />
            </button>
          </form>
        </div>
      </div>
      <Attribution />
    </div>
  );
}

export default Home;
