import makeIcon from "../../utils/makeIcon";

import { faPaperPlane, faClock } from "@fortawesome/free-solid-svg-icons";
import { faBitbucket, faGithub } from "@fortawesome/free-brands-svg-icons";

export const PaperPlaneIcon = makeIcon(faPaperPlane);
export const ClockIcon = makeIcon(faClock);

export const GithubIcon = makeIcon(faGithub);
export const BitbucketIcon = makeIcon(faBitbucket);
