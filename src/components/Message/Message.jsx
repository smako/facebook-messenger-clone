import React, { forwardRef } from "react";
import PropTypes from "prop-types";
import "./Message.css";

import moment from "moment";
import Icon from "../../components/Icon";

const Message = forwardRef(({ username, message }, ref) => {
  const itsSelf = username === message?.username;
  const date = moment.unix(message?.timestamp?.seconds).fromNow();

  return (
    <div ref={ref} className={`message ` + (itsSelf && `self`)}>
      <div className="message-info">
        <span className="message-user">{message?.username}</span>
        <span className="message-time">
          <Icon iconName="ClockIcon" />
          {date}
        </span>
      </div>
      <div className="message-text">
        <p>{message?.text}</p>
      </div>
    </div>
  );
});

Message.propTypes = {
  username: PropTypes.string,
  message: PropTypes.object,
};

export default Message;
