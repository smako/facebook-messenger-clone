import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyCHXhOLDWC2gcHUdC89l5vANOrcWWRBEe0",
  authDomain: "facebook-messenger-clone-51670.firebaseapp.com",
  databaseURL: "https://facebook-messenger-clone-51670.firebaseio.com",
  projectId: "facebook-messenger-clone-51670",
  storageBucket: "facebook-messenger-clone-51670.appspot.com",
  messagingSenderId: "641447663072",
  appId: "1:641447663072:web:8c06fd68c5e2c1ddee4785",
  measurementId: "G-JX68XV561N",
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebase.analytics();

const db = firebaseApp.firestore();

export default db;
